package com.example.booknerd.model.remote

import com.example.booknerd.model.response.BooksDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET

interface BookNerdService {

    companion object{
        private const val BASE_URL = "https://the-dune-api.herokuapp.com"

        fun getInstance(): BookNerdService {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create()
        }
    }
    @GET("/books/100")
    suspend fun getBooks(): List<BooksDTO.Book>
}