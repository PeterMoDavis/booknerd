package com.example.booknerd.model.local.entity

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.booknerd.model.local.dao.BooksDao

@Database(entities = [Book::class], version = 1)
abstract class BooksDataBase : RoomDatabase() {

abstract fun bookDao() : BooksDao

    companion object {
        private const val DATABASE_NAME = "books.db"

        @Volatile
        private var instance : BooksDataBase? = null

        fun getInstance(context: Context): BooksDataBase {
            return instance ?: synchronized(this){
                instance ?: buildDatabase(context).also {instance = it}
            }
        }

        private fun buildDatabase(context: Context) : BooksDataBase {
            return Room.databaseBuilder(context, BooksDataBase::class.java, DATABASE_NAME).build()
        }
    }
}