package com.example.booknerd.model.local.dao

import androidx.room.*
import com.example.booknerd.model.local.entity.Book
import com.example.booknerd.model.response.BooksDTO

@Dao
interface BooksDao {
    @Query("SELECT * FROM book_table")
    suspend fun getAll(): List<Book>

    @Insert
    suspend fun insert(book: List<Book>)

    @Update
    suspend fun update(book: Book)

}