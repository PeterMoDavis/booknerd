package com.example.booknerd.model

import android.content.Context
import com.example.booknerd.model.local.entity.Book
import com.example.booknerd.model.local.entity.BooksDataBase
import com.example.booknerd.model.remote.BookNerdService
import com.example.booknerd.model.response.BooksDTO
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BookNerdRepo @Inject constructor(
    private val bookNerdService: BookNerdService,
    @ApplicationContext context: Context
) {
    val booksDao = BooksDataBase.getInstance(context).bookDao()

    suspend fun getBooks() = withContext(Dispatchers.IO) {
        val cachedBooks: List<Book> = booksDao.getAll()

        return@withContext cachedBooks.ifEmpty {
            val bookTitles: List<BooksDTO.Book> = bookNerdService.getBooks()
            val books: List<Book> = bookTitles.map { Book(title = it.title) }
            booksDao.insert(books)

            return@ifEmpty books
        }
    }
}