package com.example.booknerd.model.response


import com.google.gson.annotations.SerializedName

class BooksDTO(
    val books: List<Book>
) {
    data class Book(
        val title: String,
    )
}