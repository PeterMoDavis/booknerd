package com.example.booknerd.view.books

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.booknerd.databinding.FragmentBooksBinding
import com.example.booknerd.viewmodel.BooksViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BooksFragment : Fragment() {
    private var _binding: FragmentBooksBinding? = null
    private val binding get() = _binding!!
    private val booksViewModel by viewModels<BooksViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentBooksBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
         var sentence = ""
        booksViewModel.getBooks()
        booksViewModel.books.observe(viewLifecycleOwner) { books ->
            Log.d("books", "onViewCreated: $books ")
            books.forEach { sentence += "${it.title}\n\n " }
            binding.bookTitles.text = sentence

        }

    }

}