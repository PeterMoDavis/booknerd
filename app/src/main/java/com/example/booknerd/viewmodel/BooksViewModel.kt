package com.example.booknerd.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.booknerd.model.BookNerdRepo
import com.example.booknerd.model.local.entity.Book
import com.example.booknerd.model.response.BooksDTO
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BooksViewModel @Inject constructor(private val repo: BookNerdRepo) : ViewModel() {
    private var _books = MutableLiveData <List<Book>>()
    val books: LiveData <List<Book>> get() = _books

    fun getBooks(){
        viewModelScope.launch {
            _books.value = repo.getBooks()
        }
    }
}